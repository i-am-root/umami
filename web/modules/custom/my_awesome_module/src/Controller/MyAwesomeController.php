<?php

namespace Drupal\my_awesome_module\Controller;

use Drupal\Core\Controller\ControllerBase;

class MyAwesomeController extends ControllerBase {

  const MY_BRANCH = 'Master';

  public function title() {
    return $this->t('My awesome path');
  }

  public function build() {
    return [
      'content' => [
        '#type' => 'html_tag',
        '#tag' => 'h2',
        '#value' => $this->t('My awesome @branch', ['@branch' => self::MY_BRANCH]),
      ]
    ];
  }
}

