#!/bin/bash

# Build the project.
composer install --no-dev --no-progress --prefer-dist --no-suggest
